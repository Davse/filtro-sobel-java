package sobelantifallas;

import java.rmi.Remote;
import java.rmi.RemoteException;

//Esta interfaz es la que utilizara el servidor
//para crear su objetoRemoto que realizara la
//ejecucion de la tarea

public interface IRemote extends Remote{
	public int[][] sobel(int[][] img) throws RemoteException;
}
