package sobelantifallas;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class ServidorSobel implements Runnable{

	private static int IDCount=0;
	private int myID;
	
	public static void main(String[] args) throws Throwable {
		//INICIAMOS VARIOS SERVIDORES SOBELS		
		Thread THR;
		for(int i=0; i<4;i++){ //Para probar la tolerancia a fallas podriamos quitar algun servidor sobel y ver como responde..
			IDCount+=1;
			THR = new Thread(new ServidorSobel(IDCount));
			THR.start();
		}
	}
	
	public ServidorSobel(int ID) throws Throwable {
		myID=ID;
		Imp_IRemote remoteObject = new Imp_IRemote();
		
		Registry rmiRegistry = LocateRegistry.createRegistry(5000+myID);
		IRemote stubServer = (IRemote) UnicastRemoteObject.exportObject(remoteObject,6000+myID);
		rmiRegistry.rebind("SobelServer"+myID, stubServer);		
		System.out.println("Se inicio el servidor Sobel ID:"+myID);
	}

	@Override
	public void run() {	
		Boolean T=true;
		while(T){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
	}

}
