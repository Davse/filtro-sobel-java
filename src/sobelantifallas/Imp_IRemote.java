package sobelantifallas;

import java.rmi.RemoteException;
import java.util.Random;


public class Imp_IRemote implements IRemote{

	public int[][] sobel(int[][] img) throws RemoteException {
		System.out.println("PROCESANDO SOBEL...");
		ProcesamientoImagen PrImg=new ProcesamientoImagen();
		PrImg.setImagenFromArray(img);
		PrImg.procesarSobel();
		System.out.println("PROCESADO OK!!");
		//INSERTAMOS UN POSIBLE ERROR ALEATORIO (15% de prob. de fallar)
		int i=new Random().nextInt(100);
		if (i<=15){
			System.out.println("SERVER SOBEL ERROR"+" %"+i);				
			throw new RuntimeException("ERROR DEL SERVIDOR SOBEL");
		}
		
		return PrImg.getImagenArray();
	}
}
