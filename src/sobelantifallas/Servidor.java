package sobelantifallas;

import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class Servidor {
	final private static int cantPartes=4;
	
	private static ArrayList<Thread> THRList;
	private static int[][][] partes;
	

	public static BufferedImage iniciarServidor(BufferedImage bfImg) throws InterruptedException{
				
		System.out.println("SERVIDOR PRINCIPAL INICIADO!!");
		//DIVIDIMOS LA IMAGEN EN 4 PARTES...
		int tx=bfImg.getWidth();
		int ty=bfImg.getHeight();
		
		partes=new int[cantPartes][tx][ty/cantPartes];
		for (int i=0; i<tx; i++) {
			for (int j=0; j<ty/cantPartes; j++) {
				for (int p=0; p<cantPartes; p++){
					partes[p][i][j]=bfImg.getRGB(i, j+p*(ty/cantPartes));
				}
		    }
		}
		
		THRList=new ArrayList<Thread>();
		
		Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
		    public void uncaughtException(Thread th, Throwable ex) {
		    	int sigTh=((THRList.indexOf(th)+1) % 4)+1;
		    	System.out.println("     .EXCEPCION CAPTURADA POR EL HANDLER_EXCEPTION: " + ex.getMessage());
		    	System.out.println("     .EXCEPCION DEL Thread: " + (THRList.indexOf(th)+1));
		    	System.out.println("     .INTENTAREMOS REASIGNAR SU TRABAJO A: "+sigTh);
		    	Thread myTHR;
		    	myTHR=new Thread(new ServerThread(sigTh, partes[THRList.indexOf(th)]) );		    	
		    	myTHR.setUncaughtExceptionHandler(this);
		    	myTHR.start();
		    	THRList.set(THRList.indexOf(th), myTHR);
				esperarNuevoThread(myTHR);
		    }
		};
		
		Thread THR;
		for (int p=0; p<cantPartes; p++){	
			try{
				THR=new Thread(new ServerThread((p % 4)+1, partes[p]) );
				THRList.add(THR);
				THR.setUncaughtExceptionHandler(h);
				THR.start();
			}catch(RuntimeException e){
				System.out.println("FALLA::::: "+e.getMessage());
			}			
		}
		System.out.println("ESPERANDO TODOS LOS THs");
		for (Thread T : THRList) {
			try{				
				T.join();				
			}catch(RuntimeException e){
				e.printStackTrace();
			}
		}
		System.out.println("TERMINARON TODOS LOS THs");
		
		//INSERTAMOS LA IMAGEN 
		for (int i=0; i<tx; i++) {
			for (int j=0; j<ty/cantPartes; j++) {
				for (int p=0; p<cantPartes; p++){
					bfImg.setRGB(i, j+p*(ty/cantPartes),partes[p][i][j]);					
				}
			}
		}
		
		System.out.println("SERVIDOR PRINCIPAL FINALIZADO!!!");
		return bfImg;
	}
	
	public static void esperarNuevoThread(Thread th){
		try {
			System.out.println("___ESPERANDO A NUEVO TH");
			th.join();
			System.out.println("___NUEVO TH TERMINO..");
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
	
}
