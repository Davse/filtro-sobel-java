package solbelcentralzado;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;



public class ServidorSobel {

	/**
	 * @param args
	 * @throws RemoteException 
	 */
	public static void main(String[] args) throws RemoteException {
		//INICIAMOS SERVIDOR SOBEL
		new ServidorSobel();
	
	}
	
	public ServidorSobel() throws RemoteException {
		Imp_IRemote remoteObject = new Imp_IRemote();
		
		Registry rmiRegistry = LocateRegistry.createRegistry(5000);
		IRemote stubServer = (IRemote) UnicastRemoteObject.exportObject(remoteObject,6000);
		rmiRegistry.rebind("SobelServer", stubServer);
		
		System.out.println("Se inicio el servidor Sobel ");
		
	}

}