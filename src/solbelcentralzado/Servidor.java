package solbelcentralzado;
import java.awt.image.BufferedImage;


public class Servidor {

	public static BufferedImage iniciarServidor(BufferedImage bfImg) throws InterruptedException{
				
		System.out.println("SERVIDOR PRINCIPAL INICIADO!!");
		int tx=bfImg.getWidth();
		int ty=bfImg.getHeight();
		int [] [] img= new int [tx] [ty];
		
		for (int i=0; i<tx; i++) {
			for (int j=0; j<ty; j++) {
					img[i][j]=bfImg.getRGB(i, j);
				
		    }
		}
		
		Thread THR;
		Thread T;
			
			THR=new Thread(new ServerThread(img));
			T=THR;
			THR.start();
			T.join();
			//INSERTAMOS LA IMAGEN 
			for (int i=0; i<tx; i++) {
				for (int j=0; j<ty; j++) {
					
						bfImg.setRGB(i,j,img[i][j]);					
					
				}
			}
		
		
		System.out.println("SERVIDOR PRINCIPAL FINALIZADO!!!");
		return bfImg;
	}
	
}