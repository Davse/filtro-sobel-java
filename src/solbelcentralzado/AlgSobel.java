package solbelcentralzado;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
public class AlgSobel {
	//Imagen actual que se ha cargado
    private BufferedImage imageActual;
    
    public BufferedImage setImagen(BufferedImage bmp){
       this.imageActual=bmp;
        //Retornamos el valor
        return bmp;
    }
    
    public BufferedImage setImagenFromArray(int[][] img){
    	BufferedImage bmp=new BufferedImage(img.length,img[0].length,BufferedImage.TYPE_INT_RGB);  
        for (int i=0; i<bmp.getWidth(); i++) {
            for (int j=0; j<bmp.getHeight(); j++) {
            	bmp.setRGB(i, j, img[i][j]);
            }
        }
        this.imageActual=bmp;
        return this.imageActual;
     }
    
    public BufferedImage getImagen(){
    	return this.imageActual;
    }
    
    public int[][] getImagenArray(){
    	int[][] img=new int[this.imageActual.getWidth()][this.imageActual.getHeight()];
    	for (int i=0; i<this.imageActual.getWidth(); i++) {
            for (int j=0; j<this.imageActual.getHeight(); j++) {
            	img[i][j]=this.imageActual.getRGB(i, j);
            }
        }
        return img;
     }
    
    
    //Método que devuelve una imagen abierta desde archivo
    //Retorna un objeto BufferedImagen
    public BufferedImage abrirImagen(){
        //Creamos la variable que será devuelta (la creamos como null)
        BufferedImage bmp=null;
        //Creamos un nuevo cuadro de diálogo para seleccionar imagen
        JFileChooser selector=new JFileChooser();
        //Le damos un título
        selector.setDialogTitle("Seleccione una imagen");
        //Filtramos los tipos de archivos
        String[] strFilter=new String[3];
        strFilter[0]="jpg";strFilter[1]="gif";strFilter[2]="bmp";
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & GIF & BMP",strFilter);
        selector.setFileFilter(filtroImagen);
        //Abrimos el cuadro de diálog
        int flag=selector.showOpenDialog(null);
        //Comprobamos que pulse en aceptar
        if(flag==JFileChooser.APPROVE_OPTION){
            try {
                //Devuelve el fichero seleccionado
                File imagenSeleccionada=selector.getSelectedFile();
                //Asignamos a la variable bmp la imagen leida
                bmp = ImageIO.read(imagenSeleccionada);
            } catch (Exception e) {
            }
                 
        }
        //Asignamos la imagen cargada a la propiedad imageActual
        imageActual=bmp;
        //Retornamos el valor
        return bmp;
    }
    
    public BufferedImage escalaGrises(){
        //Variables que almacenarán los píxeles
        int mediaPixel,colorSRGB;
        Color colorAux;
                
        //Recorremos la imagen píxel a píxel
        for( int i = 0; i < imageActual.getWidth(); i++ ){
            for( int j = 0; j < imageActual.getHeight(); j++ ){
                //Almacenamos el color del píxel
                colorAux=new Color(this.imageActual.getRGB(i, j));
                //Calculamos la media de los tres canales (rojo, verde, azul)
                mediaPixel=(int)((colorAux.getRed()+colorAux.getGreen()+colorAux.getBlue())/3);
                //Cambiamos a formato sRGB
                colorSRGB=(mediaPixel << 16) | (mediaPixel << 8) | mediaPixel;
                //Asignamos el nuevo valor al BufferedImage
                imageActual.setRGB(i, j,colorSRGB);
            }
        }
        //Retornamos la imagen
        return imageActual;
    }
    
    public BufferedImage procesarSobel() {
    	//0filas 1columnas 2
        int     i, j, nW, nH, img[][];
        double  Gx[][], Gy[][], G[][];
        
        escalaGrises();
        
        Color c;
        nW = imageActual.getWidth();
        nH = imageActual.getHeight();
        img = new int[nW][nH];
        for (i=0; i<nW; i++) {
            for (j=0; j<nH; j++) {
            	//System.out.println("recolectando "+i+"x"+j);
            	c=new Color(this.imageActual.getRGB(i, j));
            	img[i][j]=c.getRed();            	
            	//System.out.println("GET "+i+"x"+j+": "+img[i][j]);
            }
        }
        //leer imagen a matriz
        Gx = new double[nW][nH];
        Gy = new double[nW][nH];
        G  = new double[nW][nH];
       // double val =0;

        for (i=0; i<nW; i++) {
          for (j=0; j<nH; j++) {
        	//System.out.println("calculando nuevo "+i+"x"+j);
            if (i==0 || i==nW-1 || j==0 || j==nH-1)
              Gx[i][j] = Gy[i][j] = G[i][j] = 0; // Image boundary cleared
            else{
              Gx[i][j] = img[i+1][j-1] + 2*img[i+1][j] + img[i+1][j+1] -
                         img[i-1][j-1] - 2*img[i-1][j] - img[i-1][j+1];
              Gy[i][j] = img[i-1][j+1] + 2*img[i][j+1] + img[i+1][j+1] -
                         img[i-1][j-1] - 2*img[i][j-1] - img[i+1][j-1];
              G[i][j]= Math.abs(Gx[i][j]) + Math.abs(Gy[i][j]);
            //  System.out.print(i+"x"+j+": "+G[i][j]+" -> ");
              if (G[i][j]>255)  G[i][j]=255;
              if (G[i][j]<0)  G[i][j]=0;
             // System.out.println(G[i][j]);
            		  
            //  =img[i][j]+i;
            //  G[i][j]  = Math.sqrt( Math.pow(Gx[i][j],2.0) + Math.pow(Gy[i][j],2.0) );
            }
          }
        }
        //pasar G(matriz) a imagen..
       
        for (i=0; i<nW; i++) {
            for (j=0; j<nH; j++) {
            	//System.out.println("trasladando nuevo "+i+"x"+j);
            	this.imageActual.setRGB(i, j, ((int)G[i][j] << 16) | ((int)G[i][j] << 8) | (int)G[i][j]);
            }
        }
		return this.imageActual;
      }
}
