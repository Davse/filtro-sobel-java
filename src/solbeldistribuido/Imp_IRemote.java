package solbeldistribuido;

import java.rmi.RemoteException;


public class Imp_IRemote implements IRemote{

	public int[][] sobel(int[][] img) throws RemoteException {
		System.out.println("PROCESANDO SOBEL...");
		AlgSobel PrImg=new AlgSobel();
		PrImg.setImagenFromArray(img);
		PrImg.procesarSobel();
		System.out.println("PROCESADO OK!!");
		return PrImg.getImagenArray();
	}
}