package solbeldistribuido;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class ServerThread implements Runnable{
	int toSobelServerID;
	int[][] imagen;
	
	public ServerThread(int IDSobelServer, int[][] img){
		this.toSobelServerID =IDSobelServer;
		this.imagen=img;
	}
	
	public void run() {		
		try {
			
			Registry registry = LocateRegistry.getRegistry("127.0.0.1", 5000+toSobelServerID);
			IRemote myIRemote = (IRemote) registry.lookup("SobelServer"+toSobelServerID);
			System.out.println("Thread conectado a SOBEL."+toSobelServerID);
			
			int[][] result=myIRemote.sobel(imagen);
			for (int i=0; i<result.length; i++) {
				for (int j=0; j<result[0].length; j++) {
					imagen[i][j]=result[i][j];
				}
			}
			
			System.out.println("Thread "+toSobelServerID+" finalizo OK!");
			
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (AccessException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}	
	}

}