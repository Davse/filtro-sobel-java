package solbeldistribuido;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class Servidor {
	final private static int cantPartes=64;// cantidad de partes en que se fracciona la imagen

	public static BufferedImage iniciarServidor(BufferedImage bfImg) throws InterruptedException{
				
		System.out.println("SERVIDOR PRINCIPAL INICIADO!!");
		//DIVIDIMOS LA IMAGEN EN 4 PARTES...
		int tx=bfImg.getWidth();
		int ty=bfImg.getHeight();
		long startTime = System.nanoTime();
		
		int[][][] partes=new int[cantPartes][tx][ty/cantPartes];
		for (int i=0; i<tx; i++) {
			for (int j=0; j<ty/cantPartes; j++) {
				for (int p=0; p<cantPartes; p++){
					partes[p][i][j]=bfImg.getRGB(i, j+p*(ty/cantPartes));
				}
		    }
		}
		
		ArrayList<Thread> THRList=new ArrayList<Thread>();
		Thread THR;
		
		for (int p=0; p<cantPartes; p++){	
			THR=new Thread(new ServerThread((p % 4)+1, partes[p]) );
			THRList.add(THR);
			THR.start();
		}
		
		for (Thread T : THRList) {
			T.join();
		}
		
		long endTime = System.nanoTime();
		System.out.println("Tiempo en ms: "+(endTime-startTime)/1000000);
		//INSERTAMOS LA IMAGEN 
		for (int i=0; i<tx; i++) {
			for (int j=0; j<ty/cantPartes; j++) {
				for (int p=0; p<cantPartes; p++){
					bfImg.setRGB(i, j+p*(ty/cantPartes),partes[p][i][j]);					
				}
			}
		}
		
		
		System.out.println("SERVIDOR PRINCIPAL FINALIZADO!!!");
		return bfImg;
	}
	
}