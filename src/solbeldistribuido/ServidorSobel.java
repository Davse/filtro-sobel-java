package solbeldistribuido;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;



public class ServidorSobel {

	/**
	 * @param args
	 * @throws RemoteException 
	 */
	public static void main(String[] args) throws RemoteException {
		//INICIAMOS VARIOS SERVIDORES SOBELS
		new ServidorSobel(1);
		new ServidorSobel(2);
		new ServidorSobel(3);
		new ServidorSobel(4);
	}
	
	public ServidorSobel(int ID) throws RemoteException {
		Imp_IRemote remoteObject = new Imp_IRemote();
		
		Registry rmiRegistry = LocateRegistry.createRegistry(5000+ID);
		IRemote stubServer = (IRemote) UnicastRemoteObject.exportObject(remoteObject,6000+ID);
		rmiRegistry.rebind("SobelServer"+ID, stubServer);
		
		System.out.println("Se inicio el servidor Sobel ID:"+ID);
		
	}

}